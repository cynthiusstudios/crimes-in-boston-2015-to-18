# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Importing Dataset
dataset = pd.read_csv("crime.csv", encoding = "ISO-8859-1")
dataset.head()
dataset.isnull().sum()

# Which Crime Is The Most Common?
plt.figure(figsize=(20,15))
sns.countplot(x = dataset.OFFENSE_CODE_GROUP, order = dataset.OFFENSE_CODE_GROUP.value_counts().index[0:10])
plt.title("Most Common Crime")
plt.xlabel("OFFENSES")
plt.xticks(rotation=45)

# Which Districts Have the Most Common Crimes
most_common_offenses = dataset.OFFENSE_CODE_GROUP.value_counts().index[0:10]
plt.figure(figsize = (50,15))
j = 0
for i in most_common_offenses:
    j+=1
    plt.subplot(2,5, j)
    sns.countplot(x = dataset[dataset.OFFENSE_CODE_GROUP == str(i)].DISTRICT, order = dataset[dataset.OFFENSE_CODE_GROUP == str(i)].DISTRICT.value_counts().index[0:5])
    plt.title("Top 5 Districts Where " + i + " Was Comitted")
    
# Hour At Which Crimes are Committed The Most
sns.countplot(x = dataset.HOUR, order = dataset.HOUR.value_counts().index[0:24])
plt.title("Hours At Which Crimes Are Comitted The Most")

# Weekdays In Which Crimes are Committed The Most
sns.countplot(x = dataset.DAY_OF_WEEK, order = dataset.DAY_OF_WEEK.value_counts().index[0:7])
plt.title("Weekdays In Which Crimes Are Committed The Most")
plt.xticks(rotation = 30)
plt.xlabel("WEEKDAYS")

# Months In Which Crimes are Comitted The Most
sns.countplot(x = dataset.MONTH, order = dataset.MONTH.value_counts().index[0:12])
plt.title("Months In Which Crimes Are Committed The Most")
plt.xticks(rotation = 30)

# Year In Which Most Crimes Are Committed
sns.countplot(x = dataset.YEAR, order = dataset.YEAR.value_counts().index[0:4])
plt.title("Year In Which Highest Crimes Were Committed Between 2015-18")
plt.xticks(rotation = 30)